﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Diagnostics.CodeAnalysis;

namespace Lab2
{

    public enum CardType : byte 
    {
        AppleCard = 1,
        BananaCard = 2,
        GrapeCard = 3,
        LemonCard = 4,
        OrangeCard = 5,
        PeachCard = 6,
        PearCard = 7,
        PineappleCard = 8,
        PlumCard = 9,
        StrawberryCard,
        BoostCard = byte.MaxValue-1, 
        DeBoostCard = byte.MaxValue
    };

    public static class CardID_Generator
    {
        private static int ID;
        static CardID_Generator() { ID = 0; }
        public static int Get_ID() { ID += 1; return ID; }
    }

    public class Booster
    {
        public readonly int HP_Boost;
        public readonly int Time_Boost;
        public readonly int Score_Boost;
        public readonly bool Freeze_Time;
        public readonly bool Shield;

        public readonly int TimeToLive;

        public Booster(int HP_Boost, int Time_Boost, int Score_Boost, bool Freeze_Time, bool Shield, int TimeToLive)
        {
            this.HP_Boost = HP_Boost;
            this.Time_Boost = Time_Boost;
            this.Score_Boost = Score_Boost;
            this.Freeze_Time = Freeze_Time;
            this.Shield = Shield;

            this.TimeToLive = TimeToLive;
        }
        public Booster Clone() => new Booster(HP_Boost, Time_Boost, Score_Boost, Freeze_Time, Shield, TimeToLive);
        public override string ToString() => $"[HP_Boost={HP_Boost};Time_Boost={Time_Boost};Score_Boost={Score_Boost};Freeze_Time={Freeze_Time};Shield={Shield};TimeToLive={TimeToLive};]";
    }

    public class DeBooster
    {
        public readonly int Damage;
        public readonly int Time_deboost;
        public readonly int Score_Deboost;
        public readonly int AdditionalDamage;

        public readonly int TimeToLive;

        public DeBooster(int Damage, int Time_deboost, int Score_Deboost, int AdditionalDamage, int TimeToLive)
        {
            this.Damage = Damage;
            this.Time_deboost = Time_deboost;
            this.Score_Deboost = Score_Deboost;
            this.AdditionalDamage = AdditionalDamage;

            this.TimeToLive = TimeToLive;
        }
        public DeBooster Clone() => new DeBooster(Damage, Time_deboost, Score_Deboost, AdditionalDamage, TimeToLive);
        public override string ToString() => $"[Damage={Damage};Time_deboost={Time_deboost};Score_Deboost={Score_Deboost};AdditionalDamage={AdditionalDamage};TimeToLive={TimeToLive};]";
    }

    public abstract class Card : IComparable<Card>
    {
        public readonly int ID;
        public readonly CardType Type;
        public Card(int ID , CardType Type)
        {
            this.ID = ID;
            this.Type = Type;
        }
        public abstract Card Clone();
        public static int CardTypeComparer(Card c1, Card c2) => (byte)c1.Type - (byte)c2.Type;
        public int CompareTo(Card other) => this.ID.CompareTo(other.ID);
        public abstract new String ToString();
    }

    public class SimpleCard : Card
    {
        public int CardPoints { get; set; }
        public SimpleCard(int ID, CardType Type, int CardPoints) : base(ID,Type)
        {
            this.CardPoints = CardPoints;
        }
        public override Card Clone() => new SimpleCard(CardID_Generator.Get_ID(), Type, CardPoints);
        public override string ToString() => $"[ID={ID};Type={Type};CardPoints={CardPoints}]";
    }

    public class BoostCard : Card
    {
        public readonly Booster Boost;
        public BoostCard(int ID, Booster Boost) : base(ID, CardType.BoostCard)
        {
            this.Boost = Boost;
        }
        public override Card Clone() => new BoostCard(CardID_Generator.Get_ID(), Boost.Clone());
        public override string ToString() => $"[ID={ID};Type={Type};Boost={Boost}]";
    }

    public class DeBoostCard : Card
    {
        public readonly DeBooster DeBoost;
        public DeBoostCard(int ID, DeBooster DeBoost) : base(ID, CardType.DeBoostCard)
        {
            this.DeBoost = DeBoost;
        }
        public override Card Clone() => new DeBoostCard(CardID_Generator.Get_ID(), DeBoost.Clone());
        public override string ToString() => $"[ID={ID};Type={Type};DeBoost={DeBoost}]";
    }

    public class CardStack : IEquatable<CardStack>, IComparable<CardStack>, ICollection<Card>, IEnumerable<Card>
    {
        private List<Card> Cards;

        public int Count => Cards.Count;
        public bool IsReadOnly => false;
        public CardStack(CardStack other) : this(other.Cards) { }
        public CardStack(List<Card> Cards)
        {
            this.Cards = new List<Card>();
            foreach (Card SomeCard in Cards) this.Cards.Add(SomeCard);
            
        }
        public override string ToString() => Cards.ToString();
        public override int GetHashCode() => Cards.GetHashCode();
        public override bool Equals(object obj) => obj is CardStack stack ? stack == this : false;
        public bool Equals(CardStack other) => this == other;
        public int CompareTo(CardStack other) =>  this.Cards.Count - other.Cards.Count;
        public void Add(Card item) => this.Cards.Add(item);
        public void Clear()=> this.Cards.Clear();
        public bool Contains(Card item) => this.Cards.Contains(item);
        public void CopyTo(Card[] array, int arrayIndex) => this.Cards.CopyTo(array, arrayIndex);
        public bool Remove(Card item) => this.Cards.Remove(item);
        IEnumerator<Card> IEnumerable<Card>.GetEnumerator() => Cards.GetEnumerator();
        public IEnumerator GetEnumerator() => Cards.GetEnumerator();
        public static bool operator ==(CardStack a, CardStack b) => a.Cards.Equals(b.Cards);
        public static bool operator !=(CardStack a, CardStack b) => !a.Cards.Equals(b.Cards);


        public void Sort()
        {
            this.Cards.Sort();
        }
        
        public void Sort(Comparison<Card> SomeComparer)
        {
            
            this.Cards.Sort(SomeComparer);
        }

        public void Shuffle()
        {
            List<Card> ret = new List<Card>();
            Random rand = new Random();
            int i;
            while (Cards.Count > 0)
            {
                i = rand.Next(Cards.Count);
                ret.Add(Cards[i]);
                Cards.RemoveAt(i);
            }
            Cards = ret;
        }
    }

    public static class CardsGenerator
    {

        private static List<BoostCard> Generate_BoostCards(DifficultOptions Op)
        {
            List<BoostCard> ret = new List<BoostCard>();
            Random rand = new Random();
            for (int i = 0; i < Op.Boost_Count; i++)
            {
                ret.Add
                (
                    new BoostCard
                    (
                        CardID_Generator.Get_ID(),
                        new Booster
                        (
                            rand.Next(Op.Min_HP_Boost, Op.Max_HP_Boost),
                            Op.Time_Boost,
                            rand.Next(Op.Min_Score_Boost, Op.Max_Score_Boost),
                            Op.Allow_Freeze && rand.Next(0,10) < 5,
                            Op.Allow_Shields && rand.Next(0, 10) < 5,
                            Op.Boost_Live
                        )
                    )
                ); ;
            }
            return ret;
        }

        private static List<DeBoostCard> Generate_DeBoostCards(DifficultOptions Op)
        {
            List<DeBoostCard> ret = new List<DeBoostCard>();
            Random rand = new Random();
            for (int i = 0; i < Op.Deboost_Count; i++)
            {
                ret.Add
                (
                    new DeBoostCard
                    (
                        CardID_Generator.Get_ID(),
                        new DeBooster
                        (
                            rand.Next(Op.Min_Damage,Op.Max_Damage),
                            Op.Time_DeBoost,
                            Op.Min_Score_DeBoost,
                            Op.AdditionalDamage,
                            Op.DeBoost_Live
                        )
                    )
                );
            }
            return ret;
        }
        private static CardType GetRandomCardType()
        {
            var CardTypes = Enum.GetValues(typeof(CardType));
            return (CardType)CardTypes.GetValue(new Random().Next(CardTypes.Length-2));
        }
        private static List<SimpleCard> Generate_Cards(DifficultOptions Op)
        {
            List<SimpleCard> ret = new List<SimpleCard>();
            Random rand = new Random();
            int Count = Op.Cards - Op.Deboost_Count - Op.Boost_Count;
            for (int i = 0; i < Count/2; i++)
            {
                SimpleCard SomeCard = new SimpleCard(CardID_Generator.Get_ID(), GetRandomCardType(),rand.Next(Op.Min_Score, Op.Max_Score));
                ret.Add(SomeCard);
                ret.Add((SimpleCard)SomeCard.Clone());
            }
            return ret;
        }

        public static List<Card> Generate(DifficultOptions Op)
        {
            List<Card> SomeCards = new List<Card>();
            if (Op.Deboost_Count > 0) SomeCards.AddRange(Generate_DeBoostCards(Op));
            if (Op.Boost_Count > 0) SomeCards.AddRange(Generate_BoostCards(Op));
            SomeCards.AddRange(Generate_Cards(Op));
            return SomeCards;
        }
    }

    public class DifficultOptions
    {
        public readonly int Time;
        public readonly int Cards;
        public readonly int HP;
        public readonly int AdditionalDamage;
        public readonly int Max_Damage;
        public readonly int Min_Damage;
        public readonly int Max_Score_Boost;
        public readonly int Min_Score_Boost;
        public readonly int Max_Score_DeBoost;
        public readonly int Min_Score_DeBoost;
        public readonly int Time_DeBoost;
        public readonly int Time_Boost;
        public readonly int DeBoost_Live;
        public readonly int Boost_Live;
        public readonly bool Allow_Freeze;
        public readonly bool Allow_Shields;
        public readonly bool Allow_DeBoost;
        public readonly bool Allow_Boost;
        public readonly int Boost_Count;
        public readonly int Deboost_Count;
        public readonly int Max_HP_Boost;
        public readonly int Min_HP_Boost;
        public readonly int Max_Score;
        public readonly int Min_Score;


        public enum Difficulties { POTATO, EASY, MEDIUM, HARD, HARD_AS_HELL }

        public DifficultOptions() : this(Difficulties.MEDIUM) { }
        public DifficultOptions(Difficulties diff)
        {
            switch (diff)
            {
                case Difficulties.POTATO:
                    {
                        Time = 240;
                        Cards = 20;
                        HP = 1000;
                        AdditionalDamage = 10;
                        Max_Damage = 10;
                        Min_Damage = 1;
                        Min_Score_DeBoost = 1000;
                        Max_Score_DeBoost = 2500;
                        Min_Score_Boost = 1000;
                        Max_Score_Boost = 5000;
                        Time_DeBoost = 1;
                        Time_Boost = 60;
                        DeBoost_Live = 10;
                        Boost_Live = 30;
                        Allow_Boost = true;
                        Allow_DeBoost = false;
                        Allow_Shields = true;
                        Allow_Freeze = true;
                        Boost_Count = 4;
                        Deboost_Count = 0;
                        Max_HP_Boost = 50;
                        Min_HP_Boost = 0;
                        Max_Score = 1000;
                        Min_Score = 500;
                    }
                    break;
                case Difficulties.EASY:
                    {
                        Time = 180;
                        Cards = 30;
                        HP = 700;
                        AdditionalDamage = 20;
                        Max_Damage = 10;
                        Min_Damage = 5;
                        Max_Score_DeBoost = 2500;
                        Min_Score_DeBoost = 1000;
                        Max_Score_Boost = 7500;
                        Min_Score_Boost = 2000;
                        Time_DeBoost = 10;
                        Time_Boost = 60;
                        DeBoost_Live = 10;
                        Boost_Live = 25;
                        Allow_Boost = true;
                        Allow_DeBoost = true;
                        Allow_Shields = true;
                        Allow_Freeze = false;
                        Boost_Count = 8;
                        Deboost_Count = 2;
                        Max_HP_Boost = 20;
                        Min_HP_Boost = 10;
                        Max_Score = 2000;
                        Min_Score = 1000;
                    }
                    break;
                case Difficulties.MEDIUM:
                    {
                        Time = 160;
                        Cards = 40;
                        HP = 700;
                        AdditionalDamage = 30;
                        Max_Damage = 50;
                        Min_Damage = 20;
                        Max_Score_DeBoost = 5000;
                        Min_Score_DeBoost = 1000;
                        Max_Score_Boost = 10000;
                        Min_Score_Boost = 3000;
                        Time_DeBoost = 15;
                        Time_Boost = 60;
                        DeBoost_Live = 15;
                        Boost_Live = 25;
                        Allow_Boost = true;
                        Allow_DeBoost = true;
                        Allow_Shields = true;
                        Allow_Freeze = true;
                        Boost_Count = 8;
                        Deboost_Count = 2;
                        Max_HP_Boost = 50;
                        Min_HP_Boost = 0;
                        Max_Score = 4000;
                        Min_Score = 2000;
                    }
                    break;
                case Difficulties.HARD:
                    {
                        Time = 120;
                        Cards = 40;
                        HP = 700;
                        AdditionalDamage = 30;
                        Max_Damage = 50;
                        Min_Damage = 20;
                        Max_Score_DeBoost = 5000;
                        Min_Score_DeBoost = 1000;
                        Max_Score_Boost = 15000;
                        Min_Score_Boost = 5000;
                        Time_DeBoost = 15;
                        Time_Boost = 60;
                        DeBoost_Live = 15;
                        Boost_Live = 25;
                        Allow_Boost = true;
                        Allow_DeBoost = true;
                        Allow_Shields = true;
                        Allow_Freeze = true;
                        Boost_Count = 8;
                        Deboost_Count = 4;
                        Max_HP_Boost = 40;
                        Min_HP_Boost = 0;
                        Max_Score = 8000;
                        Min_Score = 4000;
                    }
                    break;
                case Difficulties.HARD_AS_HELL:
                    {
                        Time = 160;
                        Cards = 50;
                        HP = 700;
                        AdditionalDamage = 30;
                        Max_Damage = 50;
                        Min_Damage = 20;
                        Max_Score_DeBoost = 5000;
                        Min_Score_DeBoost = 1000;
                        Max_Score_Boost = 40000;
                        Min_Score_Boost = 20000;
                        Time_DeBoost = 15;
                        Time_Boost = 60;
                        DeBoost_Live = 15;
                        Boost_Live = 25;
                        Allow_Boost = true;
                        Allow_DeBoost = true;
                        Allow_Shields = false;
                        Allow_Freeze = false;
                        Boost_Count = 8;
                        Deboost_Count = 4;
                        Max_HP_Boost = 50;
                        Min_HP_Boost = 0;
                        Max_Score = 10000;
                        Min_Score = 5000;
                    }
                    break;
            }
        }
    }


    public static class MyExtensionMethods 
    {
        //Власний метод роширення для типів які реалізують інтерфейс IEnumerable, суть - перемішати об'єкти в колекції
        public static void Show(this IEnumerable eObj)
        {
            foreach (var C in eObj) Console.WriteLine($"{C.ToString()}");
        }
    }

    class Program
    {
        public static void Main()
        {
            //===============================================
            //Створюємо стопку карт з випадковими значеннями для заданої складності
            CardStack MyCards = new CardStack(CardsGenerator.Generate(new DifficultOptions(DifficultOptions.Difficulties.HARD_AS_HELL)));
            
            MyCards.Shuffle(); //Перемішуємо
            
            foreach (Card C in MyCards)//Показуємо карти
                Console.WriteLine($"{C.ToString()}");
            
            Console.WriteLine($"Count: {MyCards.Count}");//Показуємо кількість карт
            //=============================================== 

            //===============================================
            //Вибираємо карточки з позитивними ефектами - проектуємо в новий тип і сортуємо по додаткових балах
            Console.WriteLine($"");
            Console.WriteLine($"//===============================================");

            var BoostCards =
                from SomeCard in MyCards
                where SomeCard.Type == CardType.BoostCard
                orderby SomeCard is BoostCard SomeBoostCard ? SomeBoostCard.Boost.Score_Boost : SomeCard.ID
                select new { SomeCard.ID, ((BoostCard)SomeCard).Boost.Score_Boost };

            foreach (var C in BoostCards) Console.WriteLine($"{C.ToString()}");
            //=============================================== 
            //Вибираємо карти з яблочком і сортуємо по oчках використовуючи розширюючі методи IEnumerable
            Console.WriteLine($"");
            Console.WriteLine($"//===============================================");

            var AppleCards = 
                MyCards.
                Where(SomeCard => SomeCard.Type == CardType.AppleCard).
                OrderBy(SomeCard => SomeCard is SimpleCard sc ? sc.CardPoints : SomeCard.ID ).
                Select(SomeCard=> SomeCard);

            foreach (Card C in AppleCards) Console.WriteLine($"{C.ToString()}");
            //===============================================
            //Для завдання - 3) "Операції як з списком List так і з словником Dictionary" - фoрмую List та Dictionary
            Console.WriteLine($"");
            Console.WriteLine($"//===============================================");

            List<Card> MyCardList = MyCards.ToList(); //Формуємо список
            Dictionary<int,Card> MyCardDictionary = MyCards.ToDictionary(MyCards=> MyCards.ID);//Формуємо словник
            
            //foreach (Card C in MyCardList) Console.WriteLine($"{C.ToString()}");
            //foreach (KeyValuePair<int, Card> kvp in MyCardDictionary) Console.WriteLine($"Key: {kvp.Key}, Value:{kvp.Value}");

            var DeBoostCards = //Вибираємо DeBoost - Карточки та формуємо анонімне представлення в вигляді: [ІД] - [Значення DeBoost]
                from SomeDeBoostCard
                in MyCardList //працюємо з List
                where SomeDeBoostCard is DeBoostCard ? true : false
                select new { ((DeBoostCard)SomeDeBoostCard).ID, ((DeBoostCard)SomeDeBoostCard).DeBoost};

            foreach (var C in DeBoostCards) Console.WriteLine($"{C.ToString()}");
            Console.WriteLine($"");
            var CardsFromDictionary = //Вибираємо карточки з парним ІД, закидуємо їхні типи в результуючу множину
                from SomeCard
                in MyCardDictionary //працюємо з Dictionary
                where SomeCard.Key % 2 == 0
                select new { SomeCard.Value.Type };
            
            CardsFromDictionary.Show();

            //===============================================
            Console.WriteLine($"");
            Console.WriteLine($"//===============================================");
            MyCards.Sort();//Сортуємо по ІД карти
            foreach (Card C in MyCards)//Показуємо карти
                Console.WriteLine($"{C.ToString()}");
            Console.WriteLine($"");
            MyCards.Sort(Card.CardTypeComparer);//Сортуємо по Type карти
            foreach (Card C in MyCards)//Показуємо карти
                Console.WriteLine($"{C.ToString()}");

            //Console.WriteLine($"");
            Console.ReadLine();
        }
    }
}