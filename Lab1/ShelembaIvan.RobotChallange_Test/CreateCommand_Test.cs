﻿using System;
using Robot.Common;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class CreateCommand_Test
    {
        [TestMethod]
        public void CreateCommand_Method()
        {
            //для того щоб цей тест проходив треба в класі ShelembaIvanAlgorithm змінити константу START_ROBOT_COUNT 
            //в якій зберігається початкова кількість роботів на карті
            const int MAX_ROUNDS = 20;
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            Map map = new Map() { MaxPozition = new Position(100, 100), MinPozition = new Position(0, 0) };
            Position Station1Position = new Position(5, 5);
            Position Station2Position = new Position(7, 7);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = Station1Position, RecoveryRate = 200 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = Station2Position, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add( new Robot.Common.Robot() { Energy = 1000, Position = Station1Position, OwnerName = "Ivan" } );
            RobotCommand comm;
            comm = algorithm.DoStep(robots, 0, map);  
            Assert.IsTrue(comm is CreateNewRobotCommand);
        }
    }
}
