﻿using System;
using Robot.Common;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class TryToFreeTheStation_Test
    {
        [TestMethod]
        public void FreeStation_Test()
        {
            const int MAX_ROUNDS = 20;
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            Map map = new Map();
            Position StationPosition = new Position(5, 5);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>()
            {   new Robot.Common.Robot() { Energy = 100, Position = new Position(20,20), OwnerName = "Ivan"}
                ,new Robot.Common.Robot() { Energy = 10000, Position = new Position(30,30), OwnerName = "Ivan"}
            };
            RobotCommand comm;
            comm = algorithm.DoStep(robots, 0, map);    // Перший робот робить крок і "Бронює" станцію для себе 
            robots[0].Position = ((MoveCommand)comm).NewPosition;
            robots[0].Energy = 2;                       //моделюю випадок коли по дорозі до заброньованої станції залишилось стільки енергії що до неї не дійти
            comm = algorithm.DoStep(robots, 0, map);    //Робот повинен звільнити заброньовану станцію цією командою
           
            comm = algorithm.DoStep(robots, 1, map);    //Робот 2 повинен дійти до станції за 1 крок якщо вона вільна
            robots[1].Position = ((MoveCommand)comm).NewPosition;
            Assert.AreEqual(StationPosition , robots[1].Position);
        }
    }
}
