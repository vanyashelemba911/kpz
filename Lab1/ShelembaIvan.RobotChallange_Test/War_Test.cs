﻿using System;
using Robot.Common;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class War_Test
    {
        [TestMethod]
        public void WarTestMethod()
        {
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>()
            {   new Robot.Common.Robot() { Energy = 2000, Position = new Position(5,5), OwnerName = "Ivan"} ,
                new Robot.Common.Robot() { Energy = 100, Position = StationPosition, OwnerName = "NeIvan"} };
            var comm = algorithm.DoStep(robots, 0, map); // Дати ляща
            Assert.IsTrue(comm is MoveCommand);         
            Assert.AreEqual(((MoveCommand)comm).NewPosition, StationPosition);

        }
    }
}
