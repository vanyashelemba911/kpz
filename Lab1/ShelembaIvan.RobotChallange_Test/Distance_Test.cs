﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;


namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class Distance_Test
    {
        [TestMethod]
        public void TestMethod1()
        {
            Position a = new Position(1, 1);
            Position b = new Position(10, 2);
            Assert.AreEqual(ShelembaIvan.RobotChallange.DistanceHelper.FindDistance(a, b), 9);  
        }
    }
}
