﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;
namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class Collect_Test
    {
        [TestMethod]
        public void CollectCommand_Test()
        {
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 200, Position = StationPosition, OwnerName = "Ivan"} };
            var comm = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(comm is CollectEnergyCommand);
        }
    }
}
