﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;
namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class EnergyLoss_Test
    {
        [TestMethod]
        public void TestMethod1()
        {
            Position a = new Position(1, 1);
            Position b = new Position(10, 2);
            Assert.AreEqual(ShelembaIvan.RobotChallange.DistanceHelper.FindEnergyLoss(a, b), 81);
        }
    }
}
