﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;
namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class PathSearch_Test
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Ситуація коли робот знаходиться на такій відстані до станції що йому не вистачає енергії
            //щоб дійти до неї за один хід і треба робити декілька кроків
            //
            const int MAX_ROUNDS = 20;
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            Map map = new Map();
            Position StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 20, Position = new Position(1,1), OwnerName = "Ivan"} };
            int i = 0; RobotCommand comm;
            int k = 0; // Лічильник кроків за скільки робот дійшов до станції
            while (i < MAX_ROUNDS)
            {
                comm = algorithm.DoStep(robots,0,map);
                if (comm is MoveCommand) robots[0].Position = ((MoveCommand)comm).NewPosition;
                i++; 
                if(robots[0].Position != StationPosition) k++;
            }
            Assert.AreEqual(robots[0].Position, StationPosition);


        }
    }
}
