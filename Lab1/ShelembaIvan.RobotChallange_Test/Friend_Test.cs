﻿using System;
using Robot.Common;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class Friend_Test
    {
        [TestMethod]
        public void FriendRobot_Test()
        {
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>()
            {   new Robot.Common.Robot() { Energy = 2000, Position = new Position(5,5), OwnerName = "Ivan"} ,
                new Robot.Common.Robot() { Energy = 100, Position = new Position(9,9), OwnerName = "Ivan"} 
            };
            algorithm.DoStep(robots, 1, map);
            Assert.AreNotEqual(((MoveCommand)algorithm.DoStep(robots, 0, map)).NewPosition, StationPosition);
        }
    }
}
