﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShelembaIvan.RobotChallange_Test
{
    [TestClass]
    public class Path_Test
    {
        [TestMethod]
        public void MoveComand_Test()
        {
            //Ситуація коли робот знаходиться на такій відстані до станції що йому вистачає енергії
            //щоб стати на неї за один хід
            //
            var algorithm = new ShelembaIvan.RobotChallange.ShelembaIvanAlgorithm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var robots = new List<Robot.Common.Robot>() 
            { new Robot.Common.Robot() { Energy = 200, Position = new Position(5,5), OwnerName = "Ivan"} };
            var comm = algorithm.DoStep(robots,0,map);
            Assert.IsTrue(comm is MoveCommand);
            Assert.AreEqual( ((MoveCommand)comm).NewPosition , StationPosition);
        }
    }
}
