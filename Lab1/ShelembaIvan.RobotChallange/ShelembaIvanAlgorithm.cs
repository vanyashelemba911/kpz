﻿using System;
using System.Collections.Generic;
using Robot.Common;

namespace ShelembaIvan.RobotChallange
{
    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)Math.Sqrt(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        }
        public static int FindEnergyLoss(Position a, Position b)
        {
            return (int)(Math.Pow(FindDistance(a, b), 2));
        }
    }
    class OldPositions
    {
        public Position OldPosition;
        public int RobotId;
        public int Tick;
        public OldPositions(Position Pos, int ID)
        {
            OldPosition = Pos;
            RobotId = ID;
            Tick = 0;
        }
    }
    struct Selection
    {
        public Position SelectedPosition;
        public int RobotIndex;
        public Selection(Position Pos, int Index)
        {
            SelectedPosition = Pos;
            RobotIndex = Index;
        }
    };
    public class ShelembaIvanAlgorithm : IRobotAlgorithm
    {
        //=============Rule_Parameters_==================
        private const int ENERGY_TO_PUNCH = 50;
        private const int MAX_ROBOT_COUNT = 100;
        private const int ROUND_LIMIT_TO_CREATE = 40;
        private const int MINIMUM_STEPS_LIMIT = 50;
        private const int START_ROBOT_COUNT = 10;

        //==============For_Statistic==================
        private int RoundCount = 0;
        private int RobotCount = START_ROBOT_COUNT;
        private bool FirstStep = true;

        //==================For_Saving_Info=======================
        private IList<Selection> SelectionList = new List<Selection>();
        
        //============================================================================================
        private EnergyStation FindStation(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {   //robots[robotToMoveIndex]
            EnergyStation NearbyStation = null;
            foreach (EnergyStation SomeStation in map.Stations)
            {
                bool PickedStation = false;
                foreach (Selection PickedDots in SelectionList)
                {
                    if (SomeStation.Position.Equals(PickedDots.SelectedPosition) && robotToMoveIndex != PickedDots.RobotIndex)
                    {
                        PickedStation = true; break;
                    }
                }
                if (!PickedStation)
                {
                    if (NearbyStation == null) NearbyStation = SomeStation;
                    else
                    {
                        if (DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, SomeStation.Position) <
                            DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, NearbyStation.Position))
                            NearbyStation = SomeStation;
                    }
                }
            }
            return NearbyStation;
        }
        private EnergyStation FindStation(Position Pos, Map map)
        {   //robots[robotToMoveIndex]
            EnergyStation NearbyStation = null;
            foreach (EnergyStation SomeStation in map.Stations)
            {
                bool PickedStation = false;
                foreach (Selection PickedDots in SelectionList)
                {
                    if (SomeStation.Position.Equals(PickedDots.SelectedPosition))
                    {
                        PickedStation = true; break;
                    }
                }
                if (!PickedStation)
                {
                    if (NearbyStation == null) NearbyStation = SomeStation;
                    else
                    {
                        if (DistanceHelper.FindDistance(Pos, SomeStation.Position) <
                            DistanceHelper.FindDistance(Pos, NearbyStation.Position))
                            NearbyStation = SomeStation;
                    }
                }
            }
            return NearbyStation;
        }
        private int RobotIsHere(Position Pos, IList<Robot.Common.Robot> robots)
        {
            foreach (var SomeRobot in robots)
            {
                if (SomeRobot.Position.Equals(Pos)) return ENERGY_TO_PUNCH;
            }
            return 0;
        }
        private int MinimumSteps(int Distance, int Energy, int AnyRoboto)
        {
            if (AnyRoboto >= Energy) return MINIMUM_STEPS_LIMIT;
            Energy -= AnyRoboto;
            int i = 1;
            while ((Math.Pow((double)Distance / (double)i, 2) * i) > (Energy) && i <= MINIMUM_STEPS_LIMIT) i++;
            return i;
        }
        private Position FindPosToMove(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {   //robots[robotToMoveIndex]
            EnergyStation NearestStation = FindStation(robots, robotToMoveIndex, map);
            if (NearestStation == null)
                return robots[robotToMoveIndex].Position;

            SelectionList.Add(new Selection(NearestStation.Position, robotToMoveIndex));
            int Distance = DistanceHelper.FindDistance(NearestStation.Position, robots[robotToMoveIndex].Position);

            if (Distance * Distance + RobotIsHere(NearestStation.Position, robots) < robots[robotToMoveIndex].Energy)
                return NearestStation.Position;

            int HowMuchSteps = (int)(Distance / MinimumSteps(Distance, robots[robotToMoveIndex].Energy, RobotIsHere(NearestStation.Position, robots)));
            double X = NearestStation.Position.X - robots[robotToMoveIndex].Position.X;
            double Y = NearestStation.Position.Y - robots[robotToMoveIndex].Position.Y;
            double dx = X / (Math.Abs(X) + Math.Abs(Y)) * HowMuchSteps;
            double dy = Y / (Math.Abs(X) + Math.Abs(Y)) * HowMuchSteps;
            int RobotPosicionX = robots[robotToMoveIndex].Position.X + (int)Math.Round(dx, MidpointRounding.AwayFromZero);
            int RobotPosicionY = robots[robotToMoveIndex].Position.Y + (int)Math.Round(dy, MidpointRounding.AwayFromZero);

            return new Position(RobotPosicionX, RobotPosicionY);
        }
        
        private bool CanCreateNewRobot(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            if (RobotCount > MAX_ROBOT_COUNT || RobotCount >= map.Stations.Count || RoundCount > ROUND_LIMIT_TO_CREATE || robots[robotToMoveIndex].Energy < 255) return false;
            bool b = true;
            foreach (var SomeStation in map.Stations)
                if (robots[robotToMoveIndex].Position == SomeStation.Position)
                {
                    b = false;
                    break;
                }
            if (b) return false;
            Position NewRobotPos = map.FindFreeCell(robots[robotToMoveIndex].Position, robots);
            EnergyStation NearestStation = FindStation(NewRobotPos, map);
            int Distance = DistanceHelper.FindDistance(NearestStation.Position, NewRobotPos);

            if (Distance <= 10 && (Distance * Distance + RobotIsHere(NearestStation.Position, robots) + 250 < robots[robotToMoveIndex].Energy))
                return true;
            if (Distance > 10 && (MinimumSteps(Distance, robots[robotToMoveIndex].Energy - 250, RobotIsHere(NearestStation.Position, robots)) < 3))
                return true;
            return false;
        }
        private int CreateNewRobot(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            RobotCount++;
            return robots[robotToMoveIndex].Energy - 250;
        }
        private bool CantMove(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            if (robots[robotToMoveIndex].Energy == 0) return true;
            foreach (var SelectedStation in this.SelectionList)
            {
                if (SelectedStation.RobotIndex == robotToMoveIndex)
                {
                    int MS = this.MinimumSteps(
                        DistanceHelper.FindDistance(SelectedStation.SelectedPosition, robots[robotToMoveIndex].Position),
                        robots[robotToMoveIndex].Energy,
                        this.RobotIsHere(SelectedStation.SelectedPosition, robots));
                    if ( MS >= MINIMUM_STEPS_LIMIT) return true;
                }
            }

            return false;
        }
        private bool TryFreeTheStation(int RobotId)
        {
            for (int i = 0; i < SelectionList.Count; i++)
            {
                if (RobotId == SelectionList[i].RobotIndex)
                {
                    SelectionList.Remove( new Selection(SelectionList[i].SelectedPosition, SelectionList[i].RobotIndex));
                    return true;
                }
            }
            return true;
        }
        //==================================================================
        public ShelembaIvanAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }
        public string Author
        {
            get { return "Shelemba Ivan"; }
        }
        public string Description
        {
            get { return "Shelemba Ivan"; }
        }
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            ///////////////////////////////////////////////////
            ///                 WIN OR DIE!                 ///
            ///                   S.I.I.                    ///
            /////////////////////////////////////////////////// 

            //===========================================================================================
            
            //===========================================================================================
            if (CanCreateNewRobot(robots, robotToMoveIndex, map))
                return new CreateNewRobotCommand() { NewRobotEnergy = CreateNewRobot(robots, robotToMoveIndex, map) };
            //===========================================================================================
            foreach (var SomeStation in map.Stations)
                if (robots[robotToMoveIndex].Position == SomeStation.Position)
                    return new CollectEnergyCommand();
            //===========================================================================================
            if (CantMove(robots, robotToMoveIndex, map))
            {
                TryFreeTheStation(robotToMoveIndex);
                return new MoveCommand(){ NewPosition = robots[robotToMoveIndex].Position};
            }
            return new MoveCommand() { NewPosition = FindPosToMove(robots, robotToMoveIndex, map) };
            //===========================================================================================

        }
    }
}
