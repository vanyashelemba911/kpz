﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoNet
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = new Stopwatch();

            watch.Start();

            string connectionString = @"Data Source=DESKTOP-1OPN2DA;Initial Catalog=Cards;Integrated Security=True";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String[] Commands = 
                {
                    @"Select u.UserName, a.Name, a.Score, ua.Progress 
                    from Achivements as a
                    join UserAchivements as ua on a.ID = ua.AchicementID
                    join Users as u on u.ID = ua.UserID
                    order by u.UserName, a.Name;"
                    ,
                    @"Select u.UserName,g.Date, g.Duration 
                    from Games as g 
                    join UserPlayedGames as ug on g.ID = ug.GameID
                    join Users as u on u.ID = ug.UserID
                    order by u.UserName;"
                };

                for (int CommandNumber = 0; CommandNumber < Commands.Length; CommandNumber++)
                {
                    SqlCommand command = new SqlCommand(Commands[CommandNumber], connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            Console.Write("{0}\t\t", reader.GetName(i));
                        }
                        Console.WriteLine();

                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                Console.Write("{0}\t\t", reader.GetValue(i));
                            }
                            Console.WriteLine();
                        }
                        Console.WriteLine();
                        Console.WriteLine();
                    }
                    reader.Close();
                }
            }

            watch.Stop();

            Console.WriteLine($"Total time: { watch.ElapsedMilliseconds } ms");

            Console.Read();
        }
    }
}
