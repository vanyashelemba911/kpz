﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = new Stopwatch();
            watch.Start();

            using (CardsEntities db = new CardsEntities())
            {
                var Query1 = from u in db.Users.Include("Games") orderby u.UserName select new 
                { 
                    u.UserName,
                    u.Games
                };

                var Query2 = from a in db.Achivements
                             join ua in db.UserAchivements on a.ID equals ua.AchicementID
                             join u in db.Users on ua.UserID equals u.ID
                             orderby u.UserName, a.Name
                             select new { u.UserName, a.Name, a.Score, ua.Progress };
               
                foreach (var Q1 in Query1)
                {
                    foreach (var game in Q1.Games)
                    {
                        Console.WriteLine($" {Q1.UserName}\t{game.Date}\t{game.Duration} ");
                    }
                }

                Console.WriteLine();

                foreach (var Q2 in Query2)
                {
                    Console.WriteLine($" {Q2.UserName}\t{Q2.Name}\t{Q2.Score}\t{Q2.Progress}\t ");
                }

                Console.WriteLine();
            }

            watch.Stop();

            Console.WriteLine($"Total time: { watch.ElapsedMilliseconds } ms");

            Console.ReadLine();

        }
    }
}
