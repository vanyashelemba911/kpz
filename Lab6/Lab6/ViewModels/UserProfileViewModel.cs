﻿using Lab6.Command;
using Lab6.Models;
using Lab6.Models.DataModels;
using Lab6.Models.DataStruct;
using Lab6.NotificationObjects;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Lab6.ViewModels
{
    class UserProfileViewModel : NotificationObject
    {

        private String userName;
        private String userTeam;
        private String teamRank;
        private String teamLevel;
        private Visibility teamBlockVisability;

        #region Public properties

        public ICommand UpdateCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public ObservableCollection<AchivementInfo> Achivements { get; private set; }
        public ObservableCollection<GameStatistics> Games { get; private set; }

        public Visibility TeamBlockVisability 
        {
            get => teamBlockVisability;
            set
            {
                teamBlockVisability = value;
                OnPropertyChanged("TeamBlockVisability");
            }
        }
        public String UserName
        {
            get => userName;
            set
            {
                userName = value;
                OnPropertyChanged("UserName");
            }
        }
        public String Team
        {
            get => userTeam;
            set
            {
                userTeam = value;
                OnPropertyChanged("Team");
            }
        }
        public String TeamLevel
        {
            get => teamLevel;
            set
            {
                teamLevel = value;
                OnPropertyChanged("TeamLevel");
            }
        }
        public String TeamRank
        {
            get => teamRank;
            set
            {
                teamRank = value;
                OnPropertyChanged("TeamRank");
            }
        }

        private String realName;

        public String RealName
        {
            get { return realName; }
            set { realName = value; OnPropertyChanged("RealName"); }
        }

        private String surName;

        public String SurName
        {
            get { return surName; }
            set { surName = value; OnPropertyChanged("SurName"); }
        }

        private String bithDate;

        public String BithDate
        {
            get { return bithDate; }
            set { bithDate = value; OnPropertyChanged("BithDate"); }
        }

        private String city;

        public String City
        {
            get { return city; }
            set { city = value; OnPropertyChanged("City"); }
        }

        private String state;

        public String State
        {
            get { return state; }
            set { state = value; OnPropertyChanged("State"); }
        }

        private String country;

        public String Country
        {
            get { return country; }
            set { country = value; OnPropertyChanged("Country"); }
        }

        private Visibility loadStatusVisibility;
        public Visibility LoadStatusVisibility
        {
            get => loadStatusVisibility;
            set
            {
                loadStatusVisibility = value;
                OnPropertyChanged("LoadStatusVisibility");
            }
        }

        public PersonalDataInfo PersonalData
        {
            get
            {
                PersonalDataInfo personalData = new PersonalDataInfo();

                    personalData.City = this.City;
                    personalData.State = this.State;
                    personalData.Country = this.Country;
                    personalData.Name = this.RealName;
                    personalData.Surname = this.SurName;

                return personalData;
            }            
        }
        UserProfileModel CurrentUserProfileModel;
        #endregion

        public UserProfileViewModel(UserProfileModel userProfileModel)
        {
            CurrentUserProfileModel = userProfileModel;

            Initialize(userProfileModel);
        }

        private void UpdateUserInfo()
        {
            CurrentUserProfileModel.UpdateUserData(PersonalData);
        }

        private async void Initialize(UserProfileModel userProfileModel)
        {
            Games = new ObservableCollection<GameStatistics>();
            Achivements = new ObservableCollection<AchivementInfo>();

            DeleteCommand = new DelegateCommand(userProfileModel.DeleteUserData);
            UpdateCommand = new DelegateCommand(UpdateUserInfo);

            LoadStatusVisibility = Visibility.Visible;

            await Task.Run(()=> userProfileModel.LoadInfoFromDB());

            UserName = userProfileModel.User.UserName;
            RealName = userProfileModel.PersonalInfo.Name;
            SurName = userProfileModel.PersonalInfo.Surname;
            BithDate = userProfileModel.PersonalInfo.DateOfBirth.ToString("dd MMMM yyyy");
            City = userProfileModel.PersonalInfo.City;
            Country = userProfileModel.PersonalInfo.Country;
            State = userProfileModel.PersonalInfo.State;

            if (userProfileModel.Team == null || userProfileModel.Team.Name == null)
            {
                TeamBlockVisability = Visibility.Hidden;
            }
            else
            {
                Team = userProfileModel.Team.Name;
                TeamRank = userProfileModel.Team.TeamRank.ToString();
                TeamLevel = userProfileModel.Team.TeamLevel.ToString();
                TeamBlockVisability = Visibility.Visible;
            }

            foreach (Games gs in userProfileModel.User.Games)
            {
                GameStatistics gameStatistics = new GameStatistics();

                gameStatistics.Date = gs.Date;
                gameStatistics.Duration = gs.Duration;

                Games.Add(gameStatistics);
            }

            foreach (AchivementInfo a in userProfileModel.Achivements)
            {
                AchivementInfo achivement = new AchivementInfo();

                achivement.Name = a.Name == null ? "--" : a.Name;
                achivement.Score = a.Score == null ? 0 : a.Score;
                achivement.Progress = a.Progress == null ? 0 : a.Progress;

                Achivements.Add(achivement);
            }
            LoadStatusVisibility = Visibility.Collapsed;
        }
    }
}
