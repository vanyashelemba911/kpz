﻿using Lab6.Command;
using Lab6.Models;
using Lab6.NotificationObjects;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace Lab6.ViewModels
{
    class LoginViewModel : NotificationObject
    {
        private String login;
        private bool rememberMe;
        private String loginStatus;
        private Visibility loadingVisibility;       
        private Visibility loginStatusVisibility;

        private Func<UserInfo, bool> ValidateUserInfo;
        private Action<UserInfo> WhenUserInfoIsGood;

        public String Login 
        { 
            get => login;
            set 
            {
                login = value;
                OnPropertyChanged("Login");
            } 
        }
        public bool RememberMe
        {
            get => rememberMe;
            set
            {
                rememberMe = value;
                OnPropertyChanged("RememberMe");
            }
        }
        public Visibility LoadingVisibility
        {
            get => loadingVisibility;
            set
            {
                loadingVisibility = value;
                OnPropertyChanged("LoadingVisibility");
            }
        }
        public String LoginStatus
        {
            get => loginStatus;
            set
            {
                loginStatus = value;
                OnPropertyChanged("LoginStatus");
            }
        }
        public Visibility LoginStatusVisibility
        {
            get => loginStatusVisibility;
            set
            {
                loginStatusVisibility = value;
                OnPropertyChanged("LoginStatusVisibility");
            }
        }

        public ICommand EnterCommand { get; private set; }

        public LoginViewModel( Func<UserInfo, bool> validateUserInfo, Action<UserInfo> whenUserInfoIsGood )
        {        
            login = "";
            rememberMe = false;

            ValidateUserInfo = validateUserInfo;
            WhenUserInfoIsGood = whenUserInfoIsGood;

            LoadingVisibility = Visibility.Hidden;

            EnterCommand = new ActionCommand<Object>(EnterData);
        }

        private bool UserDataIsGood(UserInfo loginData) => loginData.LoginOrEmail != "" && loginData.Password != "";

        private async void EnterData(Object passwordValue)
        {
            UserInfo LoginData = new UserInfo();    

            LoginData.LoginOrEmail = Login;
            LoginData.Password = ((System.Windows.Controls.PasswordBox)passwordValue).Password;

            if (!UserDataIsGood(LoginData))
            {
                LoginStatus = "Login or password fields is empty!!";
                LoginStatusVisibility = Visibility.Visible;
                return;
            }

            LoadingVisibility = Visibility.Visible;

            bool ValidateStatus = await Task<bool>.Run(() => ValidateUserInfo(LoginData));
            
            if (ValidateStatus)
            {
                WhenUserInfoIsGood.Invoke(LoginData);
            }
            else
            {
                LoginStatus = "Fail, invalid login or password!"; 
                LoginStatusVisibility = Visibility.Visible;
            }

            LoadingVisibility = Visibility.Hidden;
        }        
    }
}
