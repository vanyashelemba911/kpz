﻿using Lab6.Models.Context;
using Lab6.ViewModels;
using System.Windows;
using System.Linq;
using Lab6.Models.DataModels;
using Lab6.Models;
using Lab6.Views;

namespace Lab6
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            
            Window LoginWindow = new MainWindow();

            LoginModel CurrentLoginModel = new LoginModel();

            LoginWindow.DataContext = new LoginViewModel(CurrentLoginModel.ValidateUserInfo, (UserInfo) => { ShowUserProfile(UserInfo); LoginWindow.Close(); });
            
            LoginWindow.Show();
        }
        private void ShowUserProfile(UserInfo loginData)
        {
            UserProfile UserProfileWindow = new UserProfile();

            var CurrentViewModel = new UserProfileViewModel(new UserProfileModel(loginData));

            UserProfileWindow.DataContext = CurrentViewModel;

            UserProfileWindow.Show();
        }
    }
}
