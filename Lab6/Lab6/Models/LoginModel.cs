﻿using Lab6.Models.Context;
using Lab6.Models.DataModels;

namespace Lab6.Models
{
    class LoginModel
    {
        public bool ValidateUserInfo(UserInfo userInfo)
        {
            bool Status = false;
            using (CardsDBModel dbLogin = new CardsDBModel())
            {
                foreach (LoginData loginData in dbLogin.LoginData)
                {
                    if ((loginData.Email == userInfo.LoginOrEmail ||
                        loginData.Login == userInfo.LoginOrEmail) &&
                        loginData.Password == userInfo.Password)
                    {
                        Status = true;
                        break;
                    }
                }
            }
            return Status;
        }
    }
}
