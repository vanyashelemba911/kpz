﻿using System;

namespace Lab6.Models.DataStruct
{
    class AchivementInfo
    {
        public String Name { get; set; }
        public int? Score { get; set; }
        public int? Progress { get; set; }
    }
}
