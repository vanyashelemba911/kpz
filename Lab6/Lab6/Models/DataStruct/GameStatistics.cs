﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Models.DataStruct
{
    class GameStatistics
    {
        public DateTime Date { get; set; }
        public int Duration { get; set; }
    }
}
