namespace Lab6.Models.Context
{
    using Lab6.Models.DataModels;
    using System.Data.Entity;


    public partial class CardsDBModel : DbContext
    {
        public CardsDBModel()
            : base("name=CardsDBModel")
        {
        }

        public virtual DbSet<Achivements> Achivements { get; set; }
        public virtual DbSet<Games> Games { get; set; }
        public virtual DbSet<LoginData> LoginData { get; set; }
        public virtual DbSet<PersonalData> PersonalData { get; set; }
        public virtual DbSet<Teams> Teams { get; set; }
        public virtual DbSet<UserAchivements> UserAchivements { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Achivements>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Achivements>()
                .HasMany(e => e.UserAchivements)
                .WithRequired(e => e.Achivements)
                .HasForeignKey(e => e.AchicementID);

            modelBuilder.Entity<Games>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Games)
                .Map(m => m.ToTable("UserPlayedGames").MapLeftKey("GameID").MapRightKey("UserID"));

            modelBuilder.Entity<LoginData>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<LoginData>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<LoginData>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalData>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalData>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalData>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalData>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalData>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Teams>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Teams>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Teams)
                .HasForeignKey(e => e.Team);

            modelBuilder.Entity<Users>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<Users>()
                .HasOptional(e => e.LoginData)
                .WithRequired(e => e.Users)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Users>()
                .HasOptional(e => e.PersonalData)
                .WithRequired(e => e.Users)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Users>()
                .HasMany(e => e.UserAchivements)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.UserID);
        }
    }
}
