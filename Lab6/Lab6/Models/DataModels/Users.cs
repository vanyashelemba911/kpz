namespace Lab6.Models.DataModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Users()
        {
            UserAchivements = new HashSet<UserAchivements>();
            Games = new HashSet<Games>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(32)]
        public string UserName { get; set; }

        public int TotalScore { get; set; }

        public int TotalPlayedTime { get; set; }

        public int MatchesPlayed { get; set; }

        public int OpenedCardsCount { get; set; }

        public int UsedAmplifiersCount { get; set; }

        public int? Team { get; set; }

        public virtual LoginData LoginData { get; set; }

        public virtual PersonalData PersonalData { get; set; }

        public virtual Teams Teams { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserAchivements> UserAchivements { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Games> Games { get; set; }
    }
}
