namespace Lab6.Models.DataModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class UserAchivements
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AchicementID { get; set; }

        public int? Progress { get; set; }

        public virtual Achivements Achivements { get; set; }

        public virtual Users Users { get; set; }
    }
}
