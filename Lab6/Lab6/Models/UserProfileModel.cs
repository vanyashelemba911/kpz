﻿

using Lab6.Models.Context;
using Lab6.Models.DataModels;
using Lab6.Models.DataStruct;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab6.Models
{
    class UserProfileModel
    {
        private UserInfo loginData;

        public Users User { get; private set; }
        public Teams Team { get; private set; }
        public PersonalData PersonalInfo { get; private set; }
        public List<AchivementInfo> Achivements { get; private set; }
        public UserProfileModel(UserInfo loginData)
        {
            this.loginData = loginData;
        }

        public void LoadInfoFromDB()
        {
            using (CardsDBModel db = new CardsDBModel())
            {
                int uid = (from ul in db.LoginData 
                          where ul.Login == loginData.LoginOrEmail || ul.Email == loginData.LoginOrEmail 
                          select ul.ID).FirstOrDefault();

                User = (from u in db.Users.Include("Games")
                                  where u.ID == uid
                                  select u).FirstOrDefault();

                Team = (from t in db.Teams
                                where t.ID == User.Team
                                select t).FirstOrDefault();

                PersonalInfo = (from pd in db.PersonalData
                                             where pd.ID == User.ID
                                             select pd).FirstOrDefault();

                if (PersonalInfo == null) PersonalInfo = new PersonalData();

                Achivements = (from a in db.Achivements
                               join ua in db.UserAchivements
                               on a.ID equals ua.AchicementID
                               where ua.UserID == User.ID
                               select new AchivementInfo() { Name = a.Name, Score = a.Score, Progress = ua.Progress }).ToList();
            }
        }

        public void DeleteUserData()
        {
            using (CardsDBModel db = new CardsDBModel())
            {
                db.PersonalData.Remove( (from pd in db.PersonalData where pd.ID == User.ID select pd).First() );
                db.SaveChanges();
            }
        }

        public void UpdateUserData(PersonalDataInfo newPersonalData)
        {
            
            using (CardsDBModel db = new CardsDBModel())
            {
                PersonalData userPD = db.PersonalData.Find(User.ID);

                if (userPD == null)
                {
                    userPD = new PersonalData();
                    userPD.ID = User.ID;
                    userPD.Name = newPersonalData.Name;
                    userPD.Surname = newPersonalData.Surname;
                    userPD.Country = newPersonalData.Country;
                    userPD.State = newPersonalData.State;
                    userPD.City = newPersonalData.City;
                    userPD.DateOfRegistration = DateTime.Now;
                    userPD.DateOfBirth = DateTime.Now;
                    db.PersonalData.Add(userPD);
                }
                else
                {
                    userPD.Name = newPersonalData.Name;
                    userPD.Surname = newPersonalData.Surname;
                    userPD.Country = newPersonalData.Country;
                    userPD.State = newPersonalData.State;
                    userPD.City = newPersonalData.City;
                }

                db.SaveChanges();
            }
        }
    }
}
