﻿namespace Lab6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Avatar", c => c.String(nullable: false, maxLength: 128, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Avatar");
        }
    }
}
