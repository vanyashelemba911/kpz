﻿using System;
using System.Windows.Input;

namespace Lab6.Command
{
    class ActionCommand<T> : ICommand
    {
        private readonly Action<T> _command;
        private readonly Func<bool> _canExecute;

        public event EventHandler CanExecuteChanged;

        public ActionCommand(Action<T> command, Func<bool> canExecute = null)
        {
            if (command == null) throw new ArgumentNullException("command is null in ApplicationCommand ctor");

            _canExecute = canExecute;
            _command = command;
        }

        public void Execute(object parameter) => _command?.Invoke((T)parameter);
        public bool CanExecute(object parameter) => _canExecute == null || _canExecute();
        public void RaiseCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }
}
