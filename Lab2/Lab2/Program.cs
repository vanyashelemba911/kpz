﻿using System;

namespace Lab2
{
    class Program
    {
        enum TRANSFER_STATES {UP , DOWN , NEUTRAL , REVERSE}

        partial class Engine
        {
            protected const int Start_RPM = 1500;
            protected const double Press_Gas_koef = 1.06;
            protected const double Release_Gas_koef = 0.85;

            protected String Name;
            protected int MaxRPM;
            protected int CurRPM;
            protected double Torque;
            protected bool Started;

            private int Tiks_On_MaxRPM;

            public Engine() : this("UNKNOWN",7000,100.0) { }
            public Engine(String Name,int MaxRPM, double Torque) 
            {
                this.Name = Name;
                this.Torque = Torque;
                this.MaxRPM = MaxRPM;
                CurRPM = 0;
                Started = false;
                Tiks_On_MaxRPM = 0;
            }
            public double Get_CurEnginePower()
            {
                return Torque * (double)CurRPM / 5252.0;
            }
            public int Get_CurRPM()
            {
                return CurRPM;
            }
            public void Start()
            {
                if (Started)
                {
                    Logger.ShowMessage($"Engine already started!");
                    return;
                }
                this.CurRPM = Start_RPM;
                Started = true;
                Logger.ShowMessage($"Starting Engine on {Start_RPM} RPM");
            }
            public void Press_Gas()
            {
                if (!Started) return;
                CurRPM = (int)(Press_Gas_koef * (double)CurRPM);
                if (CurRPM > MaxRPM)
                {
                    CurRPM = MaxRPM;
                    Tiks_On_MaxRPM++;
                }
                else Tiks_On_MaxRPM = 0;
                if (Tiks_On_MaxRPM > 10 && Tiks_On_MaxRPM < 15)
                {
                    Logger.ShowMessage("You ride too long on the high RPM`s!!!");
                }
                if (Tiks_On_MaxRPM >= 15)
                {
                    CutOff();
                }
            }

            public void Full_Release_Gas()
            {
                if (!Started) return;
                CurRPM = Start_RPM;
            }
            public void Release_Gas()
            {
                if (!Started) return;
                CurRPM = (int)(Release_Gas_koef * (double)CurRPM);
                if (CurRPM < Start_RPM - 500)
                {
                    Stop();
                }
            }
            public void Stop()
            {
                if (!Started)
                {
                    Logger.ShowMessage($"Engine already stopped!");
                    return;
                }
                Started = false;
                CurRPM = 0;
                Logger.ShowMessage($"Stopping the engine!");
            }
            public bool IsWorking()
            {
                return this.Started;
            }
            public String Get_Info()
            {
                return $"[ Name:{Name}; State:{Started}; CurRPM:{CurRPM}; MaxRPM:{MaxRPM}; Torque:{Torque}; Start_RPM:{Start_RPM};]";
            }
        }

        class CarSignature
        {
            public String Name { get; set; }
            public int MAX_TRANSFER { get; set; }
            public String Color { get; set; }
        }

        partial class Car
        {
            protected readonly int MAX_TRANSFER;
            protected Engine CarEngine;
            protected String Name;
            protected String Color;
            protected bool CarStarted;
            private int CurTransferVal;
            protected double CurSpeed
            {
                get
                {
                    return ((double)CurTransfer / 4 * CarEngine.Get_CurEnginePower())/2;
                }
            }
            protected int CurTransfer 
            { 
                get
                {
                    return CurTransferVal;
                }
                set
                {
                    if (value >= MAX_TRANSFER)
                        CurTransferVal = MAX_TRANSFER;
                    else
                        if (value < 0)
                            CurTransferVal = -1;
                        else
                            CurTransferVal = value;
                } 
            }
            public Car() : this(new CarSignature { Name = "UNKNOWN", MAX_TRANSFER = 5, Color = "BLACK"}, new Engine()) { }
            public bool IsStarted()
            { return CarStarted; }
            public void Turn_On()
            {
                Logger.ShowMessage("Car Turning On....");
                if (CarStarted)
                {
                    Logger.ShowMessage("Car`s already turned on!");
                    return;
                }
                CarStarted = true;
                this.CarEngine.Start();
            }
            public void Gas_pedal()
            {
                this.CarEngine.Press_Gas();
            }

            public void Break_pedal()
            {
                this.CarEngine.Release_Gas(); 
            }
            public String Get_State()
            {
                return $"Speed: {this.CurSpeed}; Transfer: {this.CurTransfer}; Engine_RPM: {CarEngine.Get_CurRPM()}";
            }
            public void Change_Transfer(TRANSFER_STATES TS)
            {
                switch (TS)
                {
                    case TRANSFER_STATES.UP :
                        {
                            Break_pedal();
                            this.CurTransfer++;
                        }
                        break;
                    case TRANSFER_STATES.DOWN:
                        {
                            Gas_pedal();
                            this.CurTransfer--;
                        }
                        break;
                    case TRANSFER_STATES.NEUTRAL:
                        {
                            CarEngine.Full_Release_Gas();
                            this.CurTransfer = 0;
                        }
                        break;
                    case TRANSFER_STATES.REVERSE:
                        {
                            if (this.CurSpeed != 0)
                            {
                                Logger.ShowMessage("Can`t set REVERSE_TRANSFER when car ride!!");
                                return;
                            }
                            CarEngine.Full_Release_Gas();
                            this.CurTransfer = -1;
                        }
                        break;
                }
            }

            public void Keep_Gas_pedal()
            {
                return;
            }

            public void Turn_Off()
            {
                Logger.ShowMessage("Car Turning Off....");
                if (!CarStarted)
                {
                    Logger.ShowMessage("Car`s already turned off!");
                    return;
                }
                CarStarted = false;
                this.CarEngine.Stop();
            }

            public void ShowCarInfo()
            {
                Console.WriteLine($"Car: [ {Name} - Engine => {CarEngine.Get_Info()}; Color => {Color}; State => {CarStarted}; CurTransfer => {CurTransfer} ]");
            }
            public int Get_CurrTransfer()
            {
                return this.CurTransfer;
            }

        }
        //======================================================================================================================================
        //              Вище не цікава реалізація класів ДВИГУН, АВТОМОБІЛЬ та інших допоміжних методів 
        //======================================================================================================================================
        static class Logger     //Клас, який виводить повідомлення на екран
        {
            public static event Action OnLogMessage;    //Подія, яка буде виконуватись при виведенні повідомлення
            static Logger() { }
            public static void ShowMessage(String Msg)
            {
                Console.WriteLine(Msg);
                if (OnLogMessage != null) OnLogMessage.Invoke();
            }
        }
        class MessagesCounter   // Клас, який буде рахувати кількість повідомлень
        {
            private int MessagesCount;
            private bool Subscribed;
            public MessagesCounter() : this(0) { }
            //Ініціалізуємо поле, та прив'язуємось до події
            public MessagesCounter(int c) { MessagesCount = c; Logger.OnLogMessage += Inc; Subscribed = true; } 
            public void Inc() //Цей метод буде викликатись в події
            {
                MessagesCount++;
            }

            public void Subscribe()
            {
                if(!Subscribed)
                    Logger.OnLogMessage += Inc;
            }
            public void UnSubscribe()
            {
                if (Subscribed)
                    Logger.OnLogMessage -= Inc;
            }

            public int Get_MessagesCount() { return this.MessagesCount; }
        }

        class CarEventArgs : EventArgs
        {
            
            public String Message { get; set; }

        }

        partial class Engine
        {
            public event EventHandler<CarEventArgs> OnCutOff;// Подія яка буде викликатись при відсічці двигуна
            private void CutOff()   //Метод який виконується при відсічці
            {
                Tiks_On_MaxRPM = 0;
                Logger.ShowMessage("Engine is CutOff!!!");
                Started = false;
                CurRPM = 0;
                Logger.ShowMessage($"Emergency stopping the engine!");
                if (this.OnCutOff != null) OnCutOff(this,new CarEventArgs() { Message = "Emergency Stopping the car!!!!!" });  //Тут ми повідомляємо за допомогою події, що наш двигун отримав відсічку
            }
        }
        partial class Car
        {
            public Car(CarSignature CS, Engine CarEngine) //Конструктор, в якому прив'язуємось до події
            {
                if (CarEngine == null)
                    CarEngine = new Engine();
                else
                    if (CarEngine.IsWorking()) //Ви поставили в нове авто працюючий двигун??????? Лол
                    {
                        CarEngine.Stop();
                    }
                if (CS == null) CS = new CarSignature { Name = "UNKNOWN", MAX_TRANSFER = 5, Color = "BLACK" };
                this.CarEngine = CarEngine;
                MAX_TRANSFER = CS.MAX_TRANSFER;
                Name = CS.Name;
                Color = CS.Color;
                CurTransfer = 0;
                CarStarted = false;
                CarEngine.OnCutOff += this.EmergencyStop;   //Привязуємось до події відсічки двигуна
            }
            private void EmergencyStop(object sender, CarEventArgs args)    //Цей метод буде викликатись при відсічці двигуна
            {
                CarStarted = false;
                this.CurTransfer = 0;
                Logger.ShowMessage(args.Message);
                Logger.ShowMessage($"sender: {sender.ToString()}");
            }
        }

        public static void Main()
        {
            const int CYCLES = 10; //коли циклів 17 і більше буде відсічка двигуна
            MessagesCounter MyMessagesCounter = new MessagesCounter();              //Буде рахувати кількість виведених повідомлень
            //Створюємо авто - Bugatti Chiron
            Car SomeCar = new Car
                (
                    new CarSignature
                    {
                        Name = "Bugatti Chiron",
                        Color = "Black",
                        MAX_TRANSFER = 6
                    },
                    new Engine
                    (
                        "W16 - 8L",
                        12000,
                        251.6
                    )
                );
            SomeCar.ShowCarInfo();                                                  //Показуємо інформацію про створений автомобіль
            SomeCar.Turn_On();                                                      //Заводимо двигун
            bool Ride = true;                                                       //
            SomeCar.Change_Transfer(TRANSFER_STATES.UP);                            //Ставимо першу передачу
            Logger.ShowMessage("Let`s ride!!");
            while (Ride)
            {
                for (int i = 0; i < CYCLES && SomeCar.IsStarted(); i++)
                {
                    SomeCar.Gas_pedal(); //тиснемо на газ
                    Logger.ShowMessage(SomeCar.Get_State());//Показуємо на екрані швидкість, передачу та оберти двигуна
                }
                Ride = SomeCar.IsStarted();//Перевірка чи авто працює
                if (SomeCar.Get_CurrTransfer() == 6) Ride = false;
                SomeCar.Change_Transfer(TRANSFER_STATES.UP);//Збільшеємо передачу
            }
            //Ок, накаталися!
            //Зупиняємось
            Ride = SomeCar.IsStarted(); //Перевірка чи авто не зламане
            if (Ride) Logger.ShowMessage("Let`s stop the car");
            while (Ride)
            {
                for (int i = 0; i < 2; i++)
                {
                    SomeCar.Break_pedal();//Знижуємо швидкість
                    Logger.ShowMessage(SomeCar.Get_State());//Показуємо на екрані швидкість, передачу та оберти двигуна
                }
                if (SomeCar.Get_CurrTransfer() == 1) Ride = false;
                SomeCar.Change_Transfer(TRANSFER_STATES.DOWN);//Зменшуємо передачу
            }
            if (SomeCar.IsStarted())//Перевірка чи авто не зламане
            {
                Logger.ShowMessage("Ok, Car is stopped ");
                SomeCar.Change_Transfer(TRANSFER_STATES.NEUTRAL);//Ставимо нейтралку
                Logger.ShowMessage("Putting NEUTRAL transfer and trying to ride");
                //Перевіримо чи поїде авто XD
                for (int i = 0; i < 3; i++)
                {
                    SomeCar.Gas_pedal();//тиснемо на газ
                    Logger.ShowMessage(SomeCar.Get_State());//Не їде? Так ми ж стоїмо на нейтральній передачі!! )))
                }
                Logger.ShowMessage("Putting REVERSE transfer and trying to ride");

                //Покатаємось задом
                SomeCar.Change_Transfer(TRANSFER_STATES.REVERSE);
                for (int i = 0; i < 10; i++)
                {
                    SomeCar.Gas_pedal();//тиснемо на газ
                    Logger.ShowMessage(SomeCar.Get_State());//Показуємо на екрані швидкість, передачу та оберти двигуна
                }
                Logger.ShowMessage("Let`s stop the car");
                //Ок, накаталися!
                //Зупиняємось
                for (int i = 0; i < 5; i++)
                {
                    SomeCar.Break_pedal();
                    Logger.ShowMessage(SomeCar.Get_State());
                }
            }
            SomeCar.Turn_Off();//Виключаємо авто
            Console.WriteLine($"{MyMessagesCounter.Get_MessagesCount()} - messages"); //Кількість виведених повідомленнь в консоль

            //Приклад підписки та відписки від повідомлень
            MessagesCounter MyCounter = new MessagesCounter();//За замовчуванням ми підписані
            for (int i = 0; i < 4; i++) Logger.ShowMessage($"Повідомлення №{i+1} - воно враховується");
            MyCounter.UnSubscribe();    //Відписуємось від події
            for (int i = 0; i < 4; i++) Logger.ShowMessage($"Повідомлення №{i+1} - воно не враховується");
            Console.WriteLine($"Враховано {MyCounter.Get_MessagesCount()} повідомлень");
            //Console.WriteLine($"");
            Console.ReadLine();
        }
    }
}
