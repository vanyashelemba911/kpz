﻿using CardsApplicationUI.ViewModels;
using CardsApplicationUI.Views;
using System.Windows;

using CardsCore;
using CardsCore.Cards;

namespace CardsApplication
{

    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {           
            Window CurrentWindow = new Window();
            Game CurrentGameSession = Game.GetInstance();
            MainWindowView CurrentView = new MainWindowView();
            MainWindowViewModel CurrentViewModel = new MainWindowViewModel();

            if (CurrentGameSession.tryToLoadUserData())
                CurrentGameSession.Initialize(20);            
            else
                CurrentGameSession.Initialize(new User("Ivan", 0), 20);

            CurrentViewModel.CurrentStatusBoardViewModel = new StatusBoardViewModel();
            CurrentViewModel.CurrentButtonsBarViewModel = new ButtonsBarViewModel();
            CurrentViewModel.CurrentCardPlaygroundViewModel = new CardPlaygroundViewModel();

            CurrentViewModel.CurrentButtonsBarViewModel.OnExitButtonClick += CurrentGameSession.SaveUserData;
            CurrentViewModel.CurrentButtonsBarViewModel.OnExitButtonClick += CurrentWindow.Close;
            
            CurrentViewModel.CurrentButtonsBarViewModel.OnPlayButtonClick += CurrentGameSession.Start;
            CurrentViewModel.CurrentButtonsBarViewModel.OnPlayButtonClick += CurrentViewModel.CurrentStatusBoardViewModel.ShowStatusBoard;
            CurrentViewModel.CurrentButtonsBarViewModel.OnPlayButtonClick += CurrentViewModel.CurrentCardPlaygroundViewModel.ShowCardPlayground;

            CurrentGameSession.OnGameStart += CurrentViewModel.CurrentCardPlaygroundViewModel.CardPlaygroundInitialize;
            CurrentGameSession.OnGameStart += CurrentViewModel.CurrentStatusBoardViewModel.StatusBoardInitialize;

            CurrentView.DataContext = CurrentViewModel;
            CurrentWindow.Content = CurrentView;
            CurrentWindow.Show();
        }
    }
}
