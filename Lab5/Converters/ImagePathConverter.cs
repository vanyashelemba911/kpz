﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace CardsApplicationUI.Converters
{
    class ImagePathConverter : IValueConverter
    {
        Dictionary<string, BitmapImage> _avatarImagesCache = new Dictionary<string, BitmapImage>();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) throw new NullReferenceException("value is null!!");

            if (value is string ImageName)
            {
                if (!_avatarImagesCache.ContainsKey(ImageName))
                {
                    Uri CardUri = new Uri(string.Format(@"..\Resources\Images\{0}.png", ImageName), UriKind.Relative);
                    _avatarImagesCache.Add(ImageName, new BitmapImage(CardUri));
                }
                return _avatarImagesCache[ImageName];
            }
            else
            {
                throw new InvalidCastException("trying to Convert not string instance to Image");
            }            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
