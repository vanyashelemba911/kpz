﻿using System.Windows.Controls;

namespace CardsApplicationUI.Views
{
    /// <summary>
    /// Interaction logic for CardPlaygroundView.xaml
    /// </summary>
    public partial class CardPlaygroundView : UserControl
    {
        public CardPlaygroundView()
        {
            InitializeComponent();
        }
    }
}
