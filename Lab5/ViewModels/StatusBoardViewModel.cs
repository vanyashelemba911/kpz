﻿using System;
using System.Windows;
using CardsApplicationUI.Commands;
using CardsCore;
using CardsCore.Cards;

namespace CardsApplicationUI.ViewModels
{
    class StatusBoardViewModel : NotificationObject
    {
        private Visibility _statusBoardVisibility;
        public Visibility StatusBoardVisibility { get => _statusBoardVisibility; }
        public String StatusBarBackground { get; set; }
        public String HPBar { get; set; }
        public String AvatarName { get; set; }
        public String UserName { get; set; }
        public int UserScore { get; set; }
        public int UserHP { get; set; }

        public StatusBoardViewModel()
        {    
            _statusBoardVisibility = Visibility.Collapsed;

            AvatarName = @"Avatars/Avatar";
            StatusBarBackground = @"Backgrounds/StatusBarBackground";
            HPBar = @"Bars/HPBar";
        }

        public void StatusBoardInitialize(Game CurrentGameSession)
        {
            UserName = CurrentGameSession.PlayerInfo.NickName;
            UserScore = CurrentGameSession.PlayerInfo.CurrentScore;

            CurrentGameSession.PlayerInfo.WhenAddScore += UpdateScore;
            CurrentGameSession.PlayerInfo.WhenApplyDamage += UpdateHP;

            OnPropertyChanged("UserName");
            OnPropertyChanged("UserScore");
        }

        public void ShowStatusBoard()
        {
            _statusBoardVisibility = Visibility.Visible;
            OnPropertyChanged("StatusBoardVisibility");
        }
        public void HideStatusBoard()
        {
            _statusBoardVisibility = Visibility.Collapsed;
            OnPropertyChanged("StatusBoardVisibility");
        }

        public void UpdateScore(int Score)
        {
            UserScore = Score;
            OnPropertyChanged("UserScore");
        }
        public void UpdateHP(int HP)
        {
            UserHP = HP;
            OnPropertyChanged("UserHP");
        }
    }

}

