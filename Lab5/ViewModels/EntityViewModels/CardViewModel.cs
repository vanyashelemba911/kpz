﻿using System;
using System.Windows;
using System.Windows.Input;
using CardsApplicationUI.Commands;
using CardsCore.Cards;

namespace CardsApplicationUI.ViewModels.EntityViewModels
{
    public class CardViewModel : NotificationObject
    {
        private Visibility _cardVisibility { get; set; }
        private string _actualTexturePath { get; set; }
        private string _textureName { get; set; }
        private string _textureFolder { get; set; }
        private int _id { get; set; }
        public Visibility CardVisibility { get => _cardVisibility; }
        public String CardTexturePath { get => _actualTexturePath; }

        public CardViewModel(Card SomeCard, int ID)
        {
            if (SomeCard == null) throw new NullReferenceException("SomeCard == null in CardViewModel ctor");
            
            _textureName = SomeCard.TypeOfCard.ToString();
            _textureFolder = "CardTextures";
            _cardVisibility = Visibility.Visible;
            _actualTexturePath = String.Format(@"{0}/{1}", _textureFolder, "HiddenCard");
            _id = ID;

            ClickOnCardCommand = new ApplicationCommand(ClickOnCard);
        }

        public event Action<int> WhenClickOnCard;
        public ICommand ClickOnCardCommand { get; }
        public void ClickOnCard() => WhenClickOnCard?.Invoke(_id);

        public void Hide()
        {
            _actualTexturePath = String.Format(@"{0}/{1}", _textureFolder, "HiddenCard");
            OnPropertyChanged("CardTexturePath");
        }
        public void Show()
        {
            _actualTexturePath = String.Format(@"{0}/{1}", _textureFolder, _textureName);
            OnPropertyChanged("CardTexturePath");
        }
        public void Delete()
        {
            _cardVisibility = Visibility.Hidden;
            OnPropertyChanged("CardVisibility");
        }

    }
}