﻿using CardsApplicationUI.Commands;
using System;
using System.Windows;

namespace CardsApplicationUI.ViewModels
{
    class MainWindowViewModel : NotificationObject
    {        
        public string BackgroundPath { get; set; }
        public StatusBoardViewModel CurrentStatusBoardViewModel { get; set; }
        public ButtonsBarViewModel CurrentButtonsBarViewModel { get; set; }
        public CardPlaygroundViewModel CurrentCardPlaygroundViewModel { get; set; }

        public MainWindowViewModel()
        {
            BackgroundPath = @"Backgrounds/Background";
        }     
    }
}
