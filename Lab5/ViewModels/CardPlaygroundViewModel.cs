﻿using CardsApplicationUI.ViewModels.EntityViewModels;
using System.Collections.ObjectModel;
using CardsCore;
using System;
using System.Windows;
using CardsApplicationUI.Commands;
using CardsCore.Cards;

namespace CardsApplicationUI.ViewModels
{
    public class CardPlaygroundViewModel : NotificationObject
    {       
        private Visibility _cardPlaygroundBoardVisibility { get; set; }
        public Visibility CardPlaygroundBoardVisibility { get => _cardPlaygroundBoardVisibility; }        
        public String CardPlaygroundBackground { get; set; }
        public ObservableCollection<CardViewModel> CurrentCards { get; }

        public CardPlaygroundViewModel()
        {
            CurrentCards = new ObservableCollection<CardViewModel>();            

            CardPlaygroundBackground = @"Backgrounds/CardPlaygroundBackground";
            _cardPlaygroundBoardVisibility = Visibility.Collapsed;
        }

        private void ShowCard(int id) => CurrentCards[id].Show();
        private void HideCard(int id) => CurrentCards[id].Hide();
        private void DeleteCard(int id) => CurrentCards[id].Delete();

        public void CardPlaygroundInitialize(Game CurrentGameSession)
        {
            if (CurrentGameSession == null) throw new NullReferenceException("CurrentGameSession == null, in CardPlaygroundViewModel ctor");

            CurrentGameSession.OnCardDelete += DeleteCard;
            CurrentGameSession.OnCardShow += ShowCard;
            CurrentGameSession.OnCardHide += HideCard;

            for (int i = 0; i < CurrentGameSession.Cards.Count; i++)
            {
                CardViewModel tempCardViewModel = new CardViewModel(CurrentGameSession.Cards[i], i);

                tempCardViewModel.WhenClickOnCard += CurrentGameSession.OpenCard;

                CurrentCards.Add(tempCardViewModel);
            }
        }
        public void ShowCardPlayground()
        {
            _cardPlaygroundBoardVisibility = Visibility.Visible;
            OnPropertyChanged("CardPlaygroundBoardVisibility");
        }
        public void HideCardPlayground()
        {
            _cardPlaygroundBoardVisibility = Visibility.Collapsed;
            OnPropertyChanged("CardPlaygroundBoardVisibility");
        }
    }
}
