﻿using CardsApplicationUI.Commands;
using System;
using System.Windows.Input;

namespace CardsApplicationUI.ViewModels
{
    public class ButtonsBarViewModel
    {
        public String ButtonsBarBackground { get; set; }
        public String ExitButtonTexturePath { get; set; }
        public String PauseButtonTexturePath { get; set; }
        public String PlayButtonTexturePath { get; set; }
        public String SettingsButtonTexturePath { get; set; }

        public ButtonsBarViewModel()
        {
            ExitButtonTexturePath = @"Buttons/close";
            PauseButtonTexturePath = @"Buttons/pause";
            PlayButtonTexturePath = @"Buttons/play";
            SettingsButtonTexturePath = @"Buttons/settings";
            ButtonsBarBackground = @"Backgrounds/ButtonsBarBackground";

            PlayButtonClickCommand = new ApplicationCommand(PlayButtonClick);
            ExitButtonClickCommand = new ApplicationCommand(ExitButtonClick);
            PauseButtonClickCommand = new ApplicationCommand(PauseButtonClick);
            SettingsButtonClickCommand = new ApplicationCommand(SettingsButtonClick);
        }

        public ICommand PlayButtonClickCommand { get; }
        public ICommand ExitButtonClickCommand { get; }
        public ICommand PauseButtonClickCommand { get; }
        public ICommand SettingsButtonClickCommand { get; }
               
        public event Action OnPlayButtonClick;
        public event Action OnExitButtonClick;
        public event Action OnPauseButtonClick;
        public event Action OnSettingsButtonClick;

        private void PlayButtonClick() => OnPlayButtonClick?.Invoke();
        private void ExitButtonClick() => OnExitButtonClick?.Invoke();
        private void PauseButtonClick() => OnPauseButtonClick?.Invoke();
        public void SettingsButtonClick() => OnSettingsButtonClick?.Invoke();

    }
}
