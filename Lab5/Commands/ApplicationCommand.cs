﻿using System;
using System.Windows.Input;

namespace CardsApplicationUI.Commands
{
    class ApplicationCommand : ICommand
    {
        private readonly Action _command;
        private readonly Func<bool> _canExecute;

        public event EventHandler CanExecuteChanged;

        public ApplicationCommand(Action command, Func<bool> canExecute = null)
        {
            if (command == null) throw new ArgumentNullException("command is null in ApplicationCommand ctor");
            
            _canExecute = canExecute;
            _command = command;
        }

        public void Execute(object parameter) => _command?.Invoke();
        public bool CanExecute(object parameter) => _canExecute == null || _canExecute();
        public void RaiseCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }
}
